:: Transcoding video for web browsers
:: 2014-07-02 Adam Walkowski - create file
:: 2014-08-17 Adam Walkowski - add presets for mobile sd transcoding
:: 2015-05-30 Adam Walkowski - add separated video and audio sources

set x264_opt=-c:v libx264 -vprofile main -preset slow -g 6 -keyint_min 6 -refs 4 -bf 16 -sc_threshold 0 -b_strategy 2 -trellis 2 -subq 8 -me_method umh -me_range 16
set x264_opt_boost=-c:v libx264 -preset slow -g 5 -keyint_min 1 -refs 4 -bf 4 -sc_threshold 0 -b_strategy 2 -trellis 2 -8x8dct 1 -subq 8 -me_method umh -me_range 24
set aac_opt=-c:a libvo_aacenc -b:a 192k -ar 44100 
set vpx_opt=-c:v libvpx -quality good -cpu-used 0 -qmin 5 -qmax 50 -g 5 -keyint_min 5 -lag-in-frames 5
set vorbis_opt =-c:a libvorbis -b:a 192k -ar 44100

set mobile_bitrate=200
set /a mobile_maxrate=5*%mobile_bitrate%
set sd_bitrate=600
set /a sd_maxrate=5*%sd_bitrate%
set hd_bitrate=1000
set /a hd_maxrate=5*%hd_bitrate%
set /a mobile_buf=5*%mobile_bitrate%
set /a sd_buf=5*%sd_bitrate%
set /a hd_buf=5*%hd_bitrate%

:: Transcoding MP4 HD (H.264 main / AAC)
ffmpeg -y -i %1 -i %2 %x264_opt_boost% -vprofile high422 -pix_fmt yuv422p -level 4.2 -b:v %hd_bitrate%k -bufsize %hd_buf%k -maxrate %hd_maxrate%k -vf scale=-1:720 -threads 60 %aac_opt% "%3.hd.mp4"

:: Transcoding MP4 SD (H.264 main / AAC)
ffmpeg -y -i %1 -i %2 %x264_opt_boost% -vprofile main -pix_fmt yuv420p -level 3.1 -b:v %sd_bitrate%k -bufsize %sd_buf%k -maxrate %sd_maxrate%k -vf scale=852:480 -threads 60 %aac_opt% "%3.sd.mp4"

:: Transcoding MP4 Mobile (H.264 main / AAC)
ffmpeg -y -i %1 -i %2 %x264_opt_boost% -vprofile main -pix_fmt yuv420p -level 2.1 -b:v %mobile_bitrate%k -bufsize %mobile_buf%k -maxrate %mobile_maxrate%k -vf scale=-1:270 -threads 60 %aac_opt% "%3.pd.mp4"

:: Transcoding WebM HD (VP8 / Vorbis)
ffmpeg -y -i %1 -i %2 %vpx_opt% -b:v %hd_bitrate%k -bufsize %hd_buf%k -maxrate %hd_maxrate%k -vf scale=-1:720 -threads 60 %vorbis_opt% -f webm "%3.hd.webm"

:: Transcoding WebM SD (VP8 / Vorbis)
ffmpeg -y -i %1 -i %2 %vpx_opt% -b:v %sd_bitrate%k -bufsize %sd_buf%k -maxrate %sd_maxrate%k -vf scale=-1:480 -threads 60 %vorbis_opt% -f webm "%3.sd.webm"

:: Transcoding WebM Mobile (VP8 / Vorbis)
ffmpeg -y -i %1 -i %2 %vpx_opt% -b:v %mobile_bitrate%k -bufsize %mobile_buf%k -maxrate %mobile_maxrate%k -vf scale=-1:270 -threads 60 %vorbis_opt% -f webm "%3.pd.webm"
