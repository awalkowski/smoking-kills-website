<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php include('controllers/LanguageController.php'); ?>

<head>
    <title><?php $c->write('meta.technology.title'); ?></title>
    <meta property="og:title" content="<?php $c->write('meta.technology.title'); ?>" />
    <meta name="description" content="<?php $c->write('meta.technology.desc'); ?>">
    <meta property="og:description" content="<?php $c->write('meta.technology.desc'); ?>" />
        
    <?php include('views/Includes.html'); ?>
</head>
<body>
   
<div id="technology-page" class="page-container">
    <?php include('views/webparts/header.html'); ?>
    
    <main id="wrapper" style="position: relative;">
        <div id="maincontentcontainer">
            
            
            <div id="maincontent" class="container">  
                
                <?php include('views/webparts/menu.html'); ?>
                
                <?php include('views/TechnologyView.html'); ?>
                
            </div> <!-- main-content END -->
        </div>
    </main>
    
    <?php include('views/webparts/footer.html'); ?>
</div>

<script type="text/javascript">    
    $(".image-box").on("click", function() {
       $('#imagepreview').attr('src', $('img', this).attr('data-path'));
       $('#imagemodal').modal('show');
    });
    
    $(".page-header-image").css({"background": "url('content/backgrounds/3.jpg')", "background-size": "cover"});
</script>
</body>
</html>