var DialogController = function(dialog) {
    var self = this,
        dialogContainer = dialog,
        dialogHeader = dialog.find('#modal-title'),
        dialogContent = dialog.find('.dialog-content');
 
     var getPlayer = function(videoUrl, width, height) {
        var content = 
            "<iframe class='embedded' width='" + width + "' height='" + height + "' " + 
            "src='" + videoUrl +
            "?autoPlay=true' frameborder='0' allowfullscreen>" +
            "</iframe>";
        return content;
    };
    
    this.show = function(videoUrl, videoWidth, videoHeight, title) {
        
        var hPadding = 40, // according to reveal css defaults
            vPadding = 30, // according to reveal css defaults
            width = videoWidth + 2 * hPadding,
            height = videoHeight + 10 + 2 * vPadding,           
            hMargin = ($(window).width() - width) / 2,
            vMargin = ($(window).height() - videoHeight) / 2,
            content = getPlayer(videoUrl, videoWidth, videoHeight);
        
        $('.reveal-modal').css({
            position: 'absolute',
            background: '#202020',
            borderRadius: '0px',
            width: videoWidth + 'px',
            height: height + 'px', 
            margin: 'auto',
            left: hMargin + 'px', 
            right: hMargin + 'px',
            top: 50 + 'px',
            bottom: -vMargin + 'px'
        });
        
        if(title.length != 0 && typeof title !== "undefined") {
            dialogHeader.append(title);   
        }
        if(content.length != 0 && typeof content !== "undefined") {
            dialogContent.append(content);    
        }
        else {
            dialogContent.append("Dialog is empty.");
        }
        
        dialogContainer.reveal().on('reveal:close', function() {
            self.close();
        });  
    };
    
    this.close = function() {
        dialogContent.empty();
        dialogHeader.empty();
    };
    
    var init = function() {
        
    };
    
    init();    
}; 
    
