<?php

abstract class LanguageHelper {
    
    protected $xml_path;
    protected $xml;
    protected $en_content_array;
    protected $pl_content_array;
 
    public function __construct($content_path) {
        $this->xml = $this->_loadXml($content_path);
        $this->en_content_array = array();
        $this->pl_content_array = array();
            
        foreach($this->xml->children() as $child) {            
            $name = $child->attributes()->__ToString();
  
            foreach($child as $key => $value) {
                //if($value != '') {    
                    switch ($key) {
                        case 'en':
                            $this->en_content_array[$name] = $value;
                            break;
                        case 'pl':
                            $this->pl_content_array[$name] = $value; 
                            break;
                        default:                       
                            break;
                    }  
                //}                  
            }
        }       
    }
    
    private function _loadXml($content_path) {
        if (filter_var($content_path, FILTER_VALIDATE_URL) === FALSE) {
            try {
                $this->xml_path = $content_path;    
                $content = html_entity_decode(file_get_contents($this->xml_path));  
                return new SimpleXMLElement($content);    
            }
            catch(exception $ex) {
                echo $ex;
            }
        }
        else {
            die("Incorrect path to the language dictionary xml.");
        }
    }   
}

?>