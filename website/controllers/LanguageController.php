<?php
    require_once('EnglishLanguageHelper.php');
    require_once('PolishLanguageHelper.php');
    
    $lang = "pl";
    if(isset($_GET['lang'])) {
        $lang = $_GET['lang'];
    }
    else {            
        $languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $lang = $languages[0];
    }
    
    $poster_path = "content/backgrounds/";
      
    if(substr($lang, 0, 3) == "en-") {
        $lang = "en";
    }
    else if($lang != "en" && $lang != "pl") {
        $lang = "en";
    }

    switch($lang) {
        case 'en':           
            $c = new EnglishLanguageHelper("content/text/smoking.content.xml");
            //$poster_path = $poster_path."smoking_poster_en.jpg";
            break;
        case 'pl': 
            $c = new PolishLanguageHelper("content/text/smoking.content.xml");
            //$poster_path = $poster_path."smoking_poster_pl.jpg";
            break;
    }   
?>