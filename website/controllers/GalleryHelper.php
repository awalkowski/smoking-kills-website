<?php

class GalleryHelper {
    
    public function getAllImageNames($dir) {
        $result = array();
            
        foreach (new DirectoryIterator($dir) as $file) {
          if ($file->isFile()) {
              $result[] = $file->getFilename();
          }
        }
        
        return $result;
    }
}

?>