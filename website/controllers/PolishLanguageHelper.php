<?php
require_once('LanguageHelper.php');

class PolishLanguageHelper extends LanguageHelper {
    public function write($name) {
        try {
            $entry = $this->pl_content_array[$name];
            echo $entry;   
        }
        catch (Exception $ex) {
            echo '';
            return $ex->getMessage();              
        }      
    }
    
    public function getText($name) {
       try {
            $entry = $this->pl_content_array[$name];
            return $entry;   
        }
        catch (Exception $ex) {
            return $ex->getMessage();              
        }  
    }
}

?>