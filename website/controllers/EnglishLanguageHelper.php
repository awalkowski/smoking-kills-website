<?php
require_once('LanguageHelper.php');

class EnglishLanguageHelper extends LanguageHelper {
    public function write($name) {
        try {
            if(array_key_exists($name, $this->en_content_array)) {
                $entry = $this->en_content_array[$name];
                echo $entry;       
            }
        }
        catch (Exception $ex) {
            echo '';          
            return $ex->getMessage();     
        }      
    }
    
    public function getText($name) {
       try {
            $entry = $this->en_content_array[$name];
            return $entry;   
        }
        catch (Exception $ex) {
            return $ex->getMessage();              
        }  
    }
}

?>