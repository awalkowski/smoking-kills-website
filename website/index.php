<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php include('controllers/LanguageController.php'); ?>
<html>
<head>
    <title><?php $c->write('meta.film.title'); ?></title>
    <meta property="og:title" content="<?php $c->write('meta.film.title'); ?>" />
    <meta name="description" content="<?php $c->write('meta.film.desc'); ?>" />
    <meta property="og:description" content="<?php $c->write('meta.film.desc'); ?>" />
    <meta name="og:image" content="http://paleniezabija.inventcore.net/content/images/headshot.jpg" />
    <meta property="og:image" content="http://paleniezabija.inventcore.net/content/images/headshot.jpg" />
    <meta property="og:image" content="http://paleniezabija.inventcore.net/content/images/headshot.jpg" />
    <?php include('views/Includes.html'); ?>
</head>
<body>
   
<div id="film-page" class="page-container">
    <?php include('views/webparts/header.html'); ?>
	
	<main id="wrapper" style="position: relative;">
	    <div id="maincontentcontainer">
            
	        
	        <div id="maincontent" class="container">  
	            
	            <?php include('views/webparts/menu.html'); ?>
	            
	            <?php include('views/FilmView.html'); ?>
	            
	        </div> <!-- main-content END -->
	    </div>
	</main>
	
	<?php include('views/webparts/footer.html'); ?>
</div>

<script type="text/javascript">
    $('.team-member').on("mouseover", function() {
        $(this).css('color', 'rgb(160, 0, 0)');
        $(this).find('.highlight-off').css({opacity: 0, border: 'solid 12px rgb(50, 50, 50)'});
        $(this).find('.highlight-on').css({border: 'solid 12px rgb(160, 0, 0)'});
    });
    $('.team-member').on("mouseout", function() {
        $(this).css('color', 'black');
        $(this).find('.highlight-off').css({opacity: 100, border: 'solid 2px rgb(50, 50, 50)'});
        $(this).find('.highlight-on').css({border: 'solid 2px rgb(50, 50, 50)'});
    });
    
    $('.tile-block').on("mouseover", function() {
        $(this).css('background', 'rgb(160, 0, 0)');
    }); 
    $('.tile-block').on("mouseout", function() {
        $(this).css('background', 'rgb(200, 200, 200)');
    });
    
    var params = window.location.href.split('?')[1];
    
    if(typeof(params) !== "undefined" && params.length > 0) {   
        var langParam = params.split('#')[0].split('=')[1];
        
        if(langParam === "pl") {
            $(".page-header-image").css({"background": "url('content/backgrounds/smoking_poster_pl.jpg')", "background-size": "cover"});
        }
        else {
            $(".page-header-image").css({"background": "url('content/backgrounds/smoking_poster_en.jpg')", "background-size": "cover"});
        }
    }
    else {
        $(".page-header-image").css({"background": "url('content/backgrounds/smoking_poster_en.jpg')", "background-size": "cover"});
    }
    
    $(document).ready(function(){
        $(".scroll").click(function(event){
            //prevent the default action for the click event
            event.preventDefault();
    
            //get the full url - like mysitecom/index.htm#home
            var full_url = this.href;
    
            //split the url by # and get the anchor target name - home in mysitecom/index.htm#home
            var parts = full_url.split("#");
            var trgt = parts[1];
    
            //get the top offset of the target anchor
            var target_offset = $("#"+trgt).offset();
            var target_top = target_offset.top;
    
            //goto that anchor by setting the body scroll top to anchor top
            $('html, body').animate({scrollTop:target_top}, 1500, 'easeInSine');
        }); 
    });
</script>
</body>
</html>